
nohup ./pox.py openflow.of_01 \
	--port=6655 forwarding.l2_learning \
	forwarding.L3Firewall --l2config="l2firewall.config" \
	--l3config="l3firewall.config" &> pox_1.log &

nohup ./pox.py openflow.of_01 \
	--port=6633 forwarding.l2_learning \
	forwarding.L3Firewall --l2config="l2firewall.config" \
	--l3config="l3firewall.config" &> pox_2.log &
